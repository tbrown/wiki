## Intro

Searching through all your different mailboxes can be cumbersome without
tools that were made for the job. A simple grep -Er will return many
headers and lines without context, apart from being really slow.

These applications index your messages in their own databases for fast
searching. Remember to schedule the update of the databases, probably
using cron.

All of them usually create a *virtual* maildir, containing symbolic
links to the original messages (or a copy in case of mbox). If one wants
to rename (for setting/clearing flags), delete or edit the mails, it is
only possible using the original message. There is currently no way to
do this in mutt, but there is a [small shell
script](http://github.com/weisslj/muttjump/raw/master/muttjump) which
can be called by a macro.

## mairix

[Mairix](http://www.rpcurnow.force9.co.uk/mairix/) is a program for
indexing and searching email messages stored in Maildir, MH or mbox
folders. The mailboxes can also be in gzip (version 0.17) or bzip2
(version 0.18) format, which makes it great for archived mailboxes.
(Check out [archivemail](http://archivemail.sourceforge.net/).)

When you perform a lookup, it creates copies or symbolic links to the
directory specified by mfolder, depending on whether you've configured
it to ouput in Maildir, mbox, MH or raw format. (the former is the
default)

     ~/.mairixrc
     base=~/.maildir
     mfolder=.Search
     database=~/.mairixdb
     maildir=.
     maildir=.lists*
     maildir=.newsletters*
     maildir=.people*
     maildir=.personal*
     maildir=.system*
     maildir=.Sent

It's important that the directory in which the results are going to be
saved is excluded from being indexed.

After configuring it, \`mairix' must be executed with no arguments so
that the database is created.

It's very annoying to have mairix complain about the existance of a
lockfile when no mairix process is running. Thus, here's ashort script
that removes it.

    ~/bin/mairix
     DB=~/.mairixdb
     trap 'rm "$DB.lock"' 1 2 3 15
     /usr/bin/mairix "$@"

mairix syntax description: <https://github.com/rc0/mairix>

## nmzmail

[nmzmail](http://www.ecademix.com/JohannesHofmann/nmzmail.html) is a
tool to use the [namazu2](http://www.namazu.org/index.html.en) search
engine from within the mutt mail reader to search mail stored in maildir
folders.

nmzmail has no configuration file, it writes its output (index, history
and search results) to ~/.nmzmail, though these values can be overridden
at runtime.

Here's a script to index your mailboxes that allows you to exclude some
of them.

    ~/bin/nmzmail-index.sh
     #!/bin/bash
     # nmzmail-index.sh - Angel Olivera <redondos@aolivera.com.ar>
     
     # Tweak MAX_MAIL_DIRS at nmzmail compile time. (default=32)
     # EXCEPTIONS is in bash glob syntax, see bash(1) for "Pathname Expansion"
     
     BASE=~/.maildir
     EXCEPTIONS="Spam Search sys*"
     
     shopt -s dotglob extglob
     EXCEPTIONS=${EXCEPTIONS// /|}
     
     for mailbox in $BASE/!(+([^.])|.+($EXCEPTIONS)); do
             ARGS[i++]="$mailbox"
     done
     
     # Uncomment to include the inbox
     # ARGS[i++]="$BASE/cur" && ARGS[i++]="$BASE/new"
     
     nmzmail -i "${ARGS[@]}"

namazu syntax description: <http://search.namazu.org/>

## mu (maildir-utils)

[mu](http://code.google.com/p/mu0/) is yet another mail indexer which
integrates well with Mutt. It is based on
[Sqlite](http://www.sqlite.org) (for mail metadata) and
[Xapian](http://www.xapian.org) (for full text search). A very brief
(and surely biased by author's final choice) among mu, mairix, and
nmzmail (discussed above) [has been blogged
about](http://upsilon.cc/~zack/blog/posts/2009/10/mail_indexing_for_mutt/).

Querying is performed using the Xapian query language.

At first, you will need to index all your Maildirs; assuming they are
all stored recursively under a single directory, you can do that with

``` 
 mu-index -m PATH
```

If the dir is ~/Maildir or some other common place (e.g. defined in
$MAILDIR), you can avoid passing -m.

Then, you can periodically update the index via cron or any other mean.
You can do that with the following cron entry:

``` 
 31  */2 *  *   *     on_ac_power && mu-index -q
```

With about 20'000 mails it is quite fast and finishes in a few seconds.

Macros for Mutt integration are reported below.

## mutt configuration

``` 
~/.mutt/macros

 ## Search mail

 # Mairix
 macro generic ,f "<shell-escape>mairix " "search via mairix"

 # Namazu/nmzmail
 macro generic ,F "<shell-escape>nmzmail -r ~/.maildir/.Search<enter>" "search via nmzmail"
 # Load the results mailbox
 macro generic ,,f "<change-folder><kill-line>=.Search<enter>" "load the search results mailbox"

 # Mu
 # first macro does the search according to user query, second macro jumps to result list
 macro index <F8> "<shell-escape>rm -rf ~/.mu/results; mu-find -o l -l ~/.mu/results " "mu-find"              
 macro index <F9> "<change-folder-readonly>~/.mu/results\n" "display mu-find results"                         
```

Of course, macros can be written to immediately change to the folder
with the results, though YMMV.
