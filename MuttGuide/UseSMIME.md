# With openssl

* [doc/smime-notes.txt](http://cvs.gnupg.org/cgi-bin/viewcvs.cgi/mutt/doc/smime-notes.txt?rev=HEAD&root=Mutt&view=markup)  
* [MuttX509](http://charles.mauch.name/documentation/MuttX509) is a walkthrough expanding on the original [Equiraptor](http://www.equiraptor.com/smime_mutt_how-to.html) documentation which covers Mutt setup and configuration, as well as CACert integration.  
* When exporting .p12 files from e.g. firefox, be sure to include all necessary intermediate CA certificates.

# With GPGME

* First, you have to build mutt with *--enable-gpgme*  
 * If your gpgme is 1.2 or newer, you have to [patch the
source](https://dev.mutt.org/trac/ticket/3300) before building. Otherwise you will see *error
creating gpgme context: Not operational*  
* Next, you should configure gpgsm:  
 * [Small HowTo on how to import freemail
S/MIME certificates into
GPGSM](https://lists.gnupg.org/pipermail/gpa-dev/2003-January/001148.html)  
 * [Tutorial for
gpgsm?](https://lists.gnupg.org/pipermail/gnupg-users/2004-September/023247.html)  
* ...and you set *crypt_use_gpgme* in your muttrc. (interactive *:set
crypt_use_gpgme* doesn't take effect)
