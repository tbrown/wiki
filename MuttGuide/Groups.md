It can be useful to categorise people into different groups, for the
purposes of quickly limiting and tagging by that group in mail folders.

To store people/group information in a separate file, source it from
your ~/.muttrc
```
# source list of people/groups  
source "~/.mutt/groups"
```

The groups file might look a bit like:
```
# friends  
group -group friends -addr Easter Bunny <eb@example.com>  
group -group friends -addr Snow White <sw@example.com>

# foes  
group -group foes -addr Dr Claw <dc@example.com>  
(..etc)
```

Then when running mutt, you can simply use eg:
```
T%f friends
```

To eg tag, all the email messages that are from anyone in your "friends"
group.

For more info see:
  - <https://muttmua.gitlab.io/mutt/manual-dev.html#addrgroup>
  - <https://muttmua.gitlab.io/mutt/manual-dev.html#patterns>
