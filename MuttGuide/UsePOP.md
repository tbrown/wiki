### Basic usage

You can use pop in 2 ways:

  - "fetchmail" function moves all (new) mail from server to local
    store.
  - URL-syntax treats remote folder as local folder with all according
    control (selective delete / move, not all at once).

to fill in: "fetchmail" verbose

to fill in: URL-syntax per [/Folders](MuttGuide/Folders)

various pop\_\* settings

use external programs to retrieve: fetchmail ...

when external, use /ProcMail to filter.

when filter, use /SpamAssassin and the like to fight spam.

### Using mutt with Gmail's POP service

Currently, running mutt with Gmail's POP service doesn't work well when
mutt is only used to browse remote mail: users will not always see all
the messages in a folder.

To fix this, you must login to Gmail from the web interface and go
'Settings' -\> 'Forwarding and POP' -\> 'POP Downloads' and select
"Enable POP for all mail (even mail that's already been downloaded)".

You might also want to change the "When messages are accessed with POP"
option according to your needs.

### Tuning

When working with POP folders, mutt can cache headers as well as whole
messages to reduce the download time greatly, see
[/Caching](MuttGuide/Caching) for details.
