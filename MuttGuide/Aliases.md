Keywords to fill:

  - alias\_file, source
  - group alias: "'''alias aliasname groupname: list of addr, addr, ...
    ''' **;**"
  - [QueryCommand](QueryCommand)
  - reverse\_alias
  - sort\_alias, alias\_format

## Mutt aliases

### Alias command format

An alias command creates a correspondence between a short name or
nickname or alias and a long name (optional) and an email address.

```
alias <nickname> [ <long name> ] <address>
```

Since the name can consist of several whitespace-separated words, the
last word is considered the address, and it can be optionally enclosed
between angle brackets.

For example: **alias mumon My dear pupil Mumon foobar@example.com**

will be parsed in this way:

    alias mumon      My dear pupil Mumon foobar@example.com
          ^          ^                   ^
          nickname   long name           email address

The nickname (or alias) will be used to select a corresponding long name
and email address when specifying the To field of an outgoing message,
e.g. when using the <mail> function in the browser or index context.

The long name is optional, so you can specify an alias command in this
way:

    alias mumon      foobar@example.com
          ^          ^
          nickname   email address

The mutt aliases mechanism works in this way: you select an alias record
from the alias menu (see below) or simply typing the nickname in the To:
prompt for an outgoing mail, then mutt will fill the To: header of the
mail in this way:

**To: \<long name\> \<email address\>**

If the alias command doesn't specify a long name, then it will fill the
To: header in this way:

**To: \<email address\>**

### Aliases management setup

You can keep all your alias commands in your muttrc file, but maybe it's
a good idea to keep them in a separate file. The $alias\_file variable
let you define this file. For example:

**set alias\_file=~/.mutt/aliases \# where I keep my aliases**

In order to load them with auto-completion you need to source the file
with:

**source ~/.mutt/aliases**

or even better (this works only with variable expansion, available in
mutt \>=1.5.12) with:

**source $alias\_file**

The **$sort\_alias** variable let you decide how you want to see the
list of aliases sorted, according to aliases or nicknames (**set
sort\_alias=alias**), to addresses (**set sort\_alias = address**) or
unsorted (**set sort\_alias= unsorted**) to keep the same order of the
aliases file.

**$alias\_format** lets you specify the format of the aliases menu
(similar to the **$folder\_format** and **$index\_format** variables).

A reasonable aliases setup could consists of the following lines:

    set alias_file= ~/.mutt/aliases
    set sort_alias= alias
    set reverse_alias=yes
    source $alias_file

### Aliases menu

A \<tab\> pressed when typing the To: field of an outgoing mail will
complete the name with the only completion or will open the alias menu
if there are more than one completions, or none: in the first case only
the possible completions will be displayed, in the second case all the
alias records will be displayed.

If you type a complete nickname in the the To: prompt and then select it
with \<enter\> then the To: field of the mail will be automatically filled
with the corresponding long name and address in this way:

**To: \<long name\> \<email address\>**

The long name specified in the alias command is what is displayed in the
To field for example as in:

**To: My dear pupil Mumon \<foobar@example.com\>**

that is it uses the following format:

**To: <long name> <address>**

If the long name isn't specified, as in:
**alias mumon \<foobar@example.com\>**
then the To: field will be filled in this way:
**To: mumon \<foobar@example.com\>**

Please note that the auto-completion performed by mutt is always
relative to the nicknames (or alias) defined by all the alias commands.

Thus for example you won't match with the auto-completion the foobar
address while pressing \<tab\> on the text: "My dear" or "pupil" or
"foobar"

but you'll do on the text: "mumon"

The function \<create-alias\> normally bound to "a" (pager and index
context), adds an alias entry in the aliases file.

From the aliases menu (or context) you can:

  - tag an entry (\<tag-entry\> function, usually bound to "t" or space)
  - select one or more entries (\<select-entry\>, usually bound to
    \<return\> or \<enter\>)
  - delete an entry (\<delete-entry\>, usually bound to "d") or undelete
    it (\<undelete-entry\>, bound to "u").

Press "?" in the alias menu to see all the current bindings.

You can delete entries in the aliases menu, but you can't edit them: for
this you need to manually edit the aliases file.

## Mutt integration with an external address book program

If you're getting hairy, for example if you want to keep a sort of
address book containing not only the information the mutt aliases file
let you track, you can use mutt in combination with an external contact
bookkeeper program like abook or ximian evolution.

For example you might get bored to have to keep the email addresses and
other contacts information (e.g. telephone numbers, sip addresses,
geographical addresses) duplicated in different record formats, and have
to manually and painfully synchronize them.

For this reason mutt has a mechanism that lets you integrate mutt with
an external address book program using a simple mechanism (easily
implemented with some scripting).

Keep in mind that such a system will be completely orthogonal to the
mutt aliases system, and the native auto-completion system will continue
to work with the native aliases system.

### Query command

In order to integrate mutt with an external program you need to define
the **$query\_command variable** (a more meaningful name could be:
**$external\_addressbook\_query\_command**).

It is the program executed when calling the function \<query\> and
\<complete-query\>, that has to take as argument a string, representing
the search string, and has to print a textual output consisting of lines
of the format:

    <email address> <tab> <long name> <tab> <other info> <newline>

This output will be parsed by mutt to fill the so called query menu with
the corresponding lines.

For example if the $query\_command is set in this way:

    set query_command = "abook --mutt-query '%s'"

the corresponding <query> function will prompt for a search string, then
will call the command:

    abook --mutt-query '<search string>'

and will display the output in the query menu.

If \<complete-query\> is called instead, it will use as the search string
what you have typed back to the last space or comma as for the alias
completion.

See also [QueryCommand](QueryCommand) for more information.

### Query menu

The query menu works in a manner slightly different from the native
aliases menu: you can't delete entries, but you can:

  - select multiple recipients (with \<tag-prefix\>, bound usually to "t")
  - perform another different query (calling again the \<query\>
    function,usually bound to "Q")
  - append the output of another query to the current one (using
    \<query-append\>, usually bound to "A")

Press the "?" key to view all the commands available when you are in a
query menu.

### Mutt and abook

A simple setup for using abook as an external addressbook program:

    set query_command = "abook --mutt-query '%s'"
    macro generic,index,pager \ca "<shell-escape>abook<return>" "launch abook"
    macro index,pager A "<pipe-message>abook --add-email<return>" "add the sender address to abook"

You can also use the abook convert facility to import the content of a
mutt aliases file into abook. This is the
    syntax:

    abook --convert [ --informat <input format>  ]  [  --infile  <input file>  ]  [ --outformat <output format> ] [ --outfile <output file> ]

It will convert \<input file\> in \<input format\> to
\<output file\> in \<output format\>.

So for
example:

``` 
 abook --convert --informat mutt --infile ~/.mutt/aliases --outformat abook --outfile ~/.abook/addressbook
```
