## run-time configuration file

Mutt reads its configuration by default 1st from a system-wide
config-file ("/etc/Muttrc"), which normally controls system settings and
provides viable default personal settings for all users, then it reads
the personal config file ("~/.muttrc" or "~/.mutt/muttrc"). This way the
personal settings override system settings as needed.

If you change the configuration, you have to restart (quit, start new)
mutt to have the changes in the configuration file be effective: mutt
doesn't reread its config files on its own. However, you can make mutt
reread them without restarting with the ":source" command.
Note that some special commands need special treatment for
(re-)initialization (like hooks unless you have "unhook" cmd specified
in your config), so a simple reread doesn't do what you expect and in
rare cases it might be *required* to restart (in particular for
"color"ing cmds).

### There are several cmd-types to control mutt configuration:

Note: the cmds given here are just for configuration. The interactive
actions (commands) which you can perform on eMails are \[run-time\]
*function*s.

#### General, internal:

##### `set var=value`

 This sets mutt-variables, of which there are a lot. See the
 manual.txt for complete reference (or
 [VarNames/List](VarNames/List)).  As you proceed throughout the
 [MuttGuide](MuttGuide) & [MuttWiki](home) you'll encounter many of
 them already.  There is also "toggle var" for yes/no values, and you
 can specify "ask-yes" or "ask-no" to be prompted each time with a
 given default answer.  You can also use "unset" or "set var" or "set
 *no*var" without a value of "yes/no" for boolean/ binary vars as a
 shortcut.  However, those makes no sense for int or string vars: mutt
 just echos the current value.  Note: the
 configuration variables for a given functionality are only available
 when you have it enabled in your binary.  Otherwise you'll get
 "*unknown variable*". Rebuild your binary and enable what you need
 per [/Setup](MuttGuide/Setup).

##### `source filename`

 When you accumulate a big personal config-file (or maintain the
 system-wide one), it's recommended to split the different configs up
 into separate files, which then can be loaded from the main
 config-file with this cmd.  In an active mutt you can (re-)execute
 mutt-config cmds via ":", so this way you can reload the whole config
 when you specify the main rc-file.  If the filename argument ends
 with "|", then the content is not evaluated literally, but executed
 as system SHELL-cmd, and the output is then taken as muttrc cmds, so
 you can dynamically change config at run-time on complex/ external
 conditions.

##### `mailboxes folder ...`

 Define what mutt should treat as "incoming mailboxes" to check for new mail.

##### `my_hdr string`

 Define standard or user-defined headers to be overwritten/ added to outgoing eMails.

#### Identities:

##### `alias short eMail-adr , ...`

 Define a shortcut for a long eMail-adr, comma separated.

##### `alternates regexp ...`

 Specify addresses by which you shall be known to mutt when checking headers for managing different addresses on same host.

##### `list regexp ...`
##### `subscribe regexp ...`

 Specify addresses to be recognized as mailing-lists, where your personal addr doesn't appear as recipient.

#### Appearance:

##### `color object foreground background [pattern]`

 Colorize elements for highlighting items.

##### `[un]ignore regexp ...`

 Trim display of headers when "weeding" them, i.e. show only desired headers.

##### `hdr_order regexp ...`

 Show headers in the specified space-separated list order.

#### Interaction:

##### `bind mode key function`

 Change key-bindings to invoke a single function later on demand.

##### `macro mode key sequence [description]`

 Bind a series of key-strikes or functions to a single key to be invoked later on demand.

#### Automation:

##### `exec function function...`

 Execute (series of) function(s) right now(!), but no input data for dialogs allowed.

##### `push sequence`

 Execute sequence of keystrikes right now(!), including input to dialogs and functions.

**Note**: you must put literal "\<\>" around function names to discern
them from a series of keys, both for "macro" & "push"
(like "`push <function1><function2>...`").
Use "`\<`"
to pass a literal "`<`" char. "exec" & "bind" go
without those literal "\<\>".

At last there are various "hooks" for automation, too, see later in
advanced usage.

### Arguments: regex vs. strings

Some config cmds as well as user interface (UI) functions take either
"\[mutt-\]pattern" or "regexp" as argument.

`regexp` are extended regular expressions like used with "sed, grep, awk" and the
like.

`"bla.*fasel"` or `"^bla$"`: string matches *anywhere* [wildcards surrounding assumed],
begin/ end must be specified with `^$`.
See "man -s 5 regex" for details and the manuals section on "Regular Expressions".

`mutt-pattern` are regexp with mutt-specific modifiers preceeding them,
like `"~f eMail.*@.*adr$"`.

### Quoting levels

regexp, mutt-pattern and the mutt-cmd as whole itself (plain string) are
3 different levels, of which each has its own set of special chars that
have to be quoted (escaped) with a \\ char prepended, when you want to
use them literally rather than serve their functions on their level.

Quoting must be checked **at each level**!!! If a char quoted on a
lower level is also special on a higher level, then the quoted char
**plus** the quoting \\ must be quoted (i.e. \\ is **mega-special**):
```
| -> \| -> \\\|
```

As you can see in the lists, not all chars are special on *every* level,
take care what you (must or need not) quote when. Don't quote when not
necessary!

Characters to quote:

regexp (see "man -a regex" for their meaning):
```
(|).*+?[]{}\^$
```

mutt-pattern (see "manual.txt" for "~" patterns and meaning of special
chars:
```
(|)!~\"' and WSPC
```

mutt-cmd (muttrc string, see manual.txt and below):
```
;#`\'"$ and WSPC
```

conflicts between lvls requiring at least double-quote (list includes
WSPC char == space + tab!):
```
(|)\$"' and WSPC
```

It seems only \\ would need "native" triple-quoting, but in combination
with complex nesting of mutt-cmds (macros, push, hooks), you can end up
with more than triple-quoting. See also [DebugConfig](DebugConfig) and [PatternQuoting](PatternQuoting).

mutt-cmd meaning:

##### `;`

 cmd separator to put multiple cmds on a single line, normally each cmd ends with newline.

##### `# comment`

 anything between "#" and an **unquoted** newline is ignored.

##### `$`
 the following legal shell-variable name is substituted (e.g. "$EDITOR").

##### `WSPC`

 (!WhiteSpace is "blank" or TAB character) WSPC characters separate arguments from cmds or patterns from each other, too!

##### ``` ` ` ```
 shell-cmd output substitution: anything between \`\` is executed in a subshell, the 1st output-line is taken as value to be used. The string is also *quoted*.

**Quoting** plain strings on mutt-cmd level and mutt-pattern:

##### `\c`

 quotes the following char "c". In the special case "\" is the **last
char (no trailing white space)** on a line,  
 it quotes the newline, which means the line continues on the next line.

##### `' '`
 every char between the ' ' is quoted except "\", which quotes itself.

##### `" "`
 every char except "\", "\`\`" & "$" is quoted, this means shell-var & -cmd
 substitution still works unless '\\'-quoted.

Note: newline continuation works even for comments "\#", contrary to the
usual behaviour in other scripting languages when you combine both.

### dynamic config

If you need to work with **conditional** processing depending on
**mutt-variables**, then you are out of luck: there is no [ScriptSupport](ScriptSupport)
in mutt. Either try using "hooks" for mutt internal processing, or use
wrapper script(s) to use external (shell-)capabilities, which then can
be used with "$SHELLVAR" or "\\".

There is experimental private mutt-vars support in the queue. You can
try it in the development version or wait for next stable release.
