This is a **sorted** list of mutt users configuration files.

More can be found
in:

* [UserPages](UserPages)   
* [ConfigTricks](ConfigTricks)   
* <http://muttrcbuilder.org/> Muttbuilder - an interactive muttrc builder. 

## Configs

* <http://dl.bulix.org/dotfiles/dot-muttrc.common>, <http://dl.bulix.org/dotfiles/dot-muttrc> - Maxime Petazzoni  
* <http://ataualpa.altervista.org/html/modules.php?name=Downloads&d_op=viewdownload&cid=2> - mutt & slrn conf files by Ataualpa the Debianer [IT]  
* <http://hermitte.free.fr/cygwin/#Mutt> - Luc Hermitte's page on Mutt+Vim on Microsoft Windows 9x+cygwin  
* <http://svn.df7cb.de/dotfiles/cb/.mutt/> - Myon's muttrc  
* <http://www.spinnaker.de/mutt/#config> - Roland Rosenfeld's configuration files.  
* <http://zeniv.linux.org.uk/~telsa/BitsAndPieces/cave.html> - Telsa Gwynne's muttrc, heavily commented and very good for beginners.  
* <http://www.burocrata.org/newwiki/MuttRc> - Tiago Macambira's muttrc (multiple accounts, per-mailbox from/header configuration)  
* <http://kinor.net/stuff/.muttrc> - Roland Buehlmann's muttrc for imap, smtp and ssl.  
* <http://www.davep.org/mutt/muttrc/> - Dave Pearson's muttrc

## Configs with screen shots

**PLEASE capture JUST the mutt window**, not the whole desktop!
:-/

* Local screenshots <http://www.mutt.org/screenshots/>   
 * To get an impression of what mutt can look like.  
* Dave Pearson: <http://www.davep.org/mutt/screenshots/>   
 * index, index with highlights, pager.  
* RadoQ: <http://xblast.sourceforge.net/rado/mutt/>  
 * columns by term-size, mailing-lists, folder-dependent settings, auto-collapsed thread view, mini-index  
* redondos: <http://github.com/redondos/mutt/tree/master/.mutt>  
 * Screen shots: (large file size.) (Thanks RadoQ for the mailbox template.)
