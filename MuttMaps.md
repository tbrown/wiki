## Maps of mutters around the world

Help us get an impression of how well mutt is spread around the globe,
add your coordinates **below** at the bottom of this page, or tell them
\`Myon\` in [MuttChannel](MuttChannel) who is responsible for updating the maps.
Currently some more users outside of North-America and Europe would be
groovy.

**The maps are for ALL of you USERS, not(!) reserved only for regulars
or developers!**

[World](http://www.df7cb.de/irc/mutt/mutt-world.html),
[Europe](http://www.df7cb.de/irc/mutt/mutt-eu.html), [Central
Europe](http://www.df7cb.de/irc/mutt/mutt-central-eu.html),
[North-America](http://www.df7cb.de/irc/mutt/mutt-na.html),
[South-America](http://www.df7cb.de/irc/mutt/mutt-sa.html).

-----

Use *decimal* coordinates; positive numbers mean north and east,
negative numbers mean south and west, resp. The comment is optional.

Find your coordinates at

`* `<http://www.multimap.com/>  
`* `<http://www.gps-data-team.com/map/>

    #!comment
     * US Residents may also use http://tiger.census.gov/cgi-bin/mapbrowse-tbl (go to bottom and enter zipcode)
      * TIGER Map service seems to be unavailable now.  Site claims to return in early 2011.

Note: the maps are manually generated occasionally. To speed things up,
ask \`Myon\` in [MuttChannel](MuttChannel).

-----

    -34.455906 -58.908329 "MatiasV" # Pilar, Buenos Aires, Argentina
    42.7 23.333333 "yordan.radunchev" # Sofia, Bulgaria
    54.102734 -3.230121 "enitharmon" # Barrow-in-Furness, Great Britain
    45.79929 10.09007 "gufo" # Iseo lake, brescia, italy
    37.776583 -122.409689 "bagueros" # san francisco, california, united states
    33.75612 -117.76534 "snappy" # Tustin, California, USA
    59.2708 17.7979 "gamkiller" # Ekerö, Stockholm, Sweden
    51.75 -1.25 "ferret" # Oxford, UK
    51.1718 4.3133 "Yaroon" # Kruibeke, Belgium
    51.1913 6.4228 "Myon" # Moenchengladbach, Germany
    53.5684 9.9745 "Rado" # Hamburg, Germany
    53.56 10.00 "grmbl" # Hamburg, Germany
    53.6257 10.12222 "ChrisH" # Hamburg, Germany
    50.77575 6.07696 "elho" # Aachen, Germany
    60 22 "ekh" # Turku, Finland
    38.0209 23.7397 "zvr" # Athens, Greece
    63.8 20.4 "winkle" # Umea, Sweden
    43.1516 -77.7393 "uberchorn"
    41.50 -81.70 "chmeee" # Cleveland, Ohio
    52.1489 9.9417 "alphascorpii" # Hildesheim
    48.8056 9.2282 "init0" # Stuttgart, Germany
    -15.844 -47.8353 "fernando" # aka musb
    47.5134 19.2015 "tibuci" # Budapest, Hungary
    55.0393 -1.4351 "Jon"
    34.0018 -118.4311 "scandal"
    42.0774 -71.3802 "JimB"
    53.2336 6.5514 "psiborg" # Groningen, The Netherlands
    32.7369 -117.2389 "ThunderChicken"
    38.15 -85.28 "Louie" # Louisville, Kentucky
    42.13 2.58 "viric" # Santa Pau, Catalonia
    51.8421 5.857 "swimagx" # Nijmegen, The Netherlands
    41.0997 29.0001 "knt"
    31.41 34.79 "nothingmuch" # Yuval Kogman, Israel
    50.13 14.48 "neuron" # Czech republic, Prague
    48.10 17.08 "r0b0" # Bratislava, SK
    51.75 -1.25 "davee" # Oxford, UK
    55.71 13.16 "pirx" # Lund, Sweden
    41.53 12.28 "intero" # Rome, Italy
    48.1756 16.4126 "cerv" # Vienna, Austria
    49.4524 -2.6309 "slarti`" # Guernsey, Channel Islands
    55.7049 12.5281 "mbp" # Copenhagen (Soeborg), Denmark
    53.5647 9.8049 "Feh" # Hamburg, Germany
    53.1535 8.2207 "jdq" # Oldenburg, Germany
    29.2979 -94.7939 "serfurj" # Galveston, Texas, USA
    52.4577 13.2985 "stucki" # Berlin(Dahlem), Germany
    -33.8177 151.0877 "fridge"
    38.15 -85.28 "gleffler" # Louisville, Kentucky
    43.0598 -71.4437 "chuck-osx" # Hooksett, New Hampshire
    49.2829 -123.1328 "brendan" # Vancouver, BC, Canada
    52.0662 4.3036 "LarstiQ" # Den Haag, The Netherlands
    29.533 -98.663 "tpope" # San Antonio, Texas, USA
    43.1 141.345 "tamo" # Sapporo, Japan
    -27.5844 151.9781 "Callipygous"
    49.60774 11.01560 "Glanzmann"
    34.002521 -84.463291 "Wayeryan" #Marietta, Georgia, USA
    37.71954 -122.441081 "brick" # SF, CA, USA
    46.8525 9.5296 "calmar" # Chur (Graubuenden), Switzerland
    47.2995 11.2199 "sklaatz" # Tirol, Austria
    53.9484 -1.0536 "`jim`" # York, UK
    50.7196 5.491 "lurch_" # Riemst, BE
    42.60 -5.57 "Teleyinex" # León, ES
    38.350 -0.483 "dato" # Alicante, Spain [ES]
    40.7285 -73.9877 "oblio" # Manhattan (East Village), New York, USA
    50.8206 8.7884 "Selket" # Marburg, Germany
    43.5195 13.2251 "Ataualpa" # Francesco Ciattaglia; Jesi, Italia
    48.6946 6.185 "|Lupin|" # Nancy, France
    40.4657 17.2344 "Rankxerox" # Stefano Longo; Taranto, Italia
    50.0036 8.2529 "ishigami" # Tobias Henle; Mainz, Hessen, Germany
    -33.9104 151.2474 "bongoman" # Richard Sandilands; Randwick, Sydney, Australia
    49.6479 6.4014 "tlr" # Luxembourg
    -43.32 172.38 "bryan" # bryan in christchurch.
    44.1073 -87.6666 "rizzo" # Don Seiler; Manitowoc, WI, USA
    43.142 -89.381 "sekhmet" # CJ Kucera; Madison, WI, USA
    53.0829 8.8273 "mangels" # Bremen, Germany
    49.2195 15.8803 "r080" # Trebic (T&#345;ebí&#269;), Czech Republic
    52.5167 13.4283 "hynek" # Berlin, Germany
    47.15655 -122.44884 "xterminus" # Tacoma, WA, USA
    49.78 8.35 "XTaran"
    41.70057 -86.242893 "philsnow" # South Bend, IN, USA
    37.9911 23.7291 "betabug" # Athens, Greece
    57.0242 9.9406 "dmal"
    35.0884 -92.4109 "ryanc" # Conway, AR, USA
    52.5101 13.4745 "pdmef" # Berlin, Germany
    50.7201 -3.5431 "James" # Exeter, UK
    29.6132 -82.3872 "markofvero" # Gainesville, FL, USA
    50.1042 11.4436 "doczook" # Tobias Mummert, found on mutt-users
    37.39920 122.13935 "karsten" # Palo Alto, CA
    -32.9477 -68.8519 "redondos" # Mendoza, Argentina
    -37.07 -56.50 "albertito" # Pinamar, Argentina
    37.961981781 23.751446307 "prasinos" # Athens, Greece
    46.6691 11.164 "fleaslob" # Meran, Italy 
    33.60 -117.18 "cannonball" # Murrieta, CA USA
    13.03395 80.17749 "kumanna" # Chennai, India
    13.03395 80.17749 "varunhiremath" # Chennai, India
    18.91931 72.74118 "aparnaappaiah" # Mumbai, India
    -37.8160 144.9667 "greentea" # Melbourne, Australia
    47.27169 -2.24733 "jcn706"   # Saint-Nazaire, France
    45.493939 -122.621348 "MichaleR"  #Portland, Oregon, USA
    38.70609 -9.16835 "ggb" #Lisboa, Portugal
    -29.68924 -51.05346 "ferhr" # Campo Bom, Brazil
    41.41417 2.1903 "mimosinnet" # Barcelona, Spain, EU
    57.71811 11.93222 "ekolin" # Göteborg, Sweden
    23.03333 113.71666 "overrider" # Dongguan, China
    48.51852 9.99047 "meillo" # Ulm, Germany
    63.899 24.495 "essal" # Sievi, Finland
    -23.487416 -46.618791 "marcioandreyoliveira" # Marcio Andrey Oliveira; Sao Paulo, SP, Brazil
    30.87117 -83.29927 "WatersOfOblivion" # Valdosta, Ga, USA
    19.43514 -99.13861 "chilicuil" # Nezahualcoyotl, Edomex. Mexico
    39.131 -84.425 "oats" # Cincinnati, Ohio, USA
    58.55 62.46667 "nes" # Nizhny Tagil, Russia
    40.878876 -74.142459 "sputnik" # Clifton, NJ, USA
    -34.52947 -58.50151 "rcoss" # Vicente Lopez, Buenos Aires, Argentina
    9.52148 51.32383 "Kays" # Goettingen, West-germany
    13.08784 80.27847 "Rishabh" # Chennai, India
    -3.74 -76.18 "kristianpaul" # Crstian-Paul, Colombia
    53.36 -6.18 "metapieman" # Dublin, Ireland
    39.83824 -75.70314 "Tarryn" # Kennett Square, PA, USA
    36.153851 -86.671443 "trix" # Nashville, TN, USA
    51.530844 -0.077100 "hoxtonhopper" # London, UK
    41.039039 28.986964 "hemlockII" # Istanbul, TR
    43.568178 1.458089 "laomaiweng" # Toulouse, France
    37.76907 -122.44388 "anon" # San Francisco, CA, USA
    39.74757 -105.2211 "jackr" # Golden, CO
    29.4748400 -98.3386460 "srart" # San Antonio, Texas, USA
    51.986229 4.364050 "sennawr"
    29.592512 -98.38992 "mbucahus" # San Antonio Texas
    -1.1935931 43.2521585 "sebd"
    60.166667 24.933333 "pyllyukko"
    27.336435 -82.530653 "rage" # Sarasota, FL, USA
