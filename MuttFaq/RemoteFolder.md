### How can I access my POP/IMAP account(s)?

The easiest/ fastest way is to use URL syntax:

`pops://user@example.com/      (pop:// - no SSL)`  
`imaps://user@example.com/path (imap:// - no SSL)`

You can specify them where ever you have to provide a folder name (like
"change-folder" or hooks). This includes even your "inbox", see
[MuttGuide/Folders](MuttGuide/Folders). This treats them as regular local folders, i.e. for
POP you can delete single a message on the server, i.e. it doesn't just
POP them off "all or none".

For "once in a while" remote access mutt's built-in support for POP/
IMAP is nice, but for frequent/ high volume POP access
[MRA]([MailConcept](MailConcept) "wikilink") like fetchmail are
recommended.

### How can I configure mutt with IMAP so that I can read a message without large attachments (eg pdf, doc, ppt) being downloaded?

When I access my IMAP server from my android phone using K-9 Mail, I can
read a message body without attachments being downloaded. In mutt, I
can't figure out how to get this behavior. Instead, my mutt is
downloading all the attachments the moment I select a message, which is
often a waste of time and
bandwidth.

### When I use a ssh-tunnel-to-IMAP to my imap server I get error messages ...

like **connection to 10.0.2.1 closed.** or **imap\_check\_capabilities
\[\]**? When I try to run the tunnel command in my shell everything
seems fine. What's going on?!

A proper description of this error can be found at [Debian Bug report
logs -
\#162859](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=162859). This
issue seems to be related to users using a ssh-tunnel-to-Courier-IMAP.
If the user "runs the courier binary directly it sends stderr along with
stdout (since it's not being run by courier's standard facilities)"

The Right way
http://blog.underdog-projects.net/2008/09/tunnel-your-imap-over-ssh/
of setting a ssh-tunnel-to-Courier-IMAP is

`set tunnel="ssh -q mail.example.com /usr/sbin/imapd 2>/dev/null Maildir"`

When using **dovecot** as imap server, try the following tunnel
setting:

`set tunnel="ssh -q mail.example.com '/usr/sbin/dovecot -c ~/.dovecot.conf --exec-mail imap'"`

You'll have to specify an alternative config file with the -c option,
since in most cases you won't be able to read system-wide configuration
file in /etc/dovecot

### How can I save typing long IMAP-pathnames?

If you have just one IMAP account and / or don't need the **$folder**
variable to access local folders or use **"~"** to do so, then you can
store the common path of a single IMAP-server in **$folder** and use
**"+" or "="** as a shortcut (see [MuttGuide/Folders](MuttGuide/Folders)).

You can specify **mailboxes** to have more than 1 listed in the mailbox
folder-browse mode (change folder, hit '?' twice to find the key to
toggle "mailboxes view"). You can use **$folder** to save typing when
specifying them (note to reset it at the end to a sane local value):

``` 
 set folder=imap://AAA.tld
 mailboxes +folder1 +folder2 ...
 set folder=imap://BBB.tld
 mailboxes +folder1 +folder2 ...
 ...
 set folder=~/Mail
```

Or use macros in the different menus and contexts (changing or saving
folders), especially if you have more than 1.

With hooks you can change "**$folder**" dynamically, get creative.

### How can I list/ browse remote IMAP-folders?

As with local folders, you can use TAB to enter folder-browse mode for
the **change-folder** command. Just specify the base directory and let
mutt TAB-expand/-browse it for you ([MuttGuide/Folders](MuttGuide/Folders)).

You can also define all your IMAP folders as "mailboxes" and then enter
"mailboxes" view. For this set "**imap\_list\_subscribed**" and
"**imap\_check\_subscribed**" as desired, then go and actually subscribe
your desired folders. Then see /Action about "mailboxes"
view.

### I'm using only IMAP folders, yet mutt still queries about local folders, why?

Because you probably have forgotten to change one of the settings for
[MuttGuide/Folders](MuttGuide/Folders), or you might have specified **mailboxes** pointing to
local
folders.

### I keep getting disconnected when I leave mutt alone for a few minutes

There is probably a firewall or NAT router between you and your server
which drops connections that don't get traffic frequently enough. Mutt
by default will send keepalive traffic every 15 minutes, which is well
within the IMAP specification (servers are required to hold connections
open for 30 minutes).

Unfortunately some routers will drop you after as little as one minute.
You can try adjusting **$timeout** (reducing this polls the current
mailbox more often), **$mail\_check** (this controls how frequently
*other* mailboxes are polled), or **$imap\_keepalive** (which controls
how often keepalive messages are sent in non-interactive contexts, eg
while composing email messages). Lower these until your router stops
disconnecting you, but don't make them so low that mutt starts to feel
sluggish.

### Why do I have to review my server's SSL certificate every single time I connect to it?

Because it's not in the system-wide certificate database. You probably
can't do anything about that, but you can set **$certificate\_file** to
the path of a file in which to store these unknown certificates. If this
is set, you'll see a new option to save certificates permanently the
next time you connect. As long as the certificate is valid (e.g. hasn't
expired), you shouldn't be nagged about it again.
