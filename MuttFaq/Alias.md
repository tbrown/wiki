### How to use the aliases (addressbook)?

In every prompt where you have to enter an address (compose menu, new
mail) you can hit TAB to get a list of defined aliases. As with all
usual TAB-completion, you may limit the hits by specifying initial
characters.

You can add new aliases either from within mutt by invoking the "create
alias" function, which defines the alias for the current mutt-session
and offers to write it to an external txt-file. The default is specified
with **$alias\_file**. If you don't save the alias to a file, it will
get lost for the next time you call mutt.

### I changed the aliases but mutt still doesn't know them?

When you edit the alias file externally with an editor or when you add
aliases via mutt and let it append them to a txt-file, make sure you use
the same file that mutt has to read when it starts!

For details: RTFM section **Defining/ Using aliases**

### Is there no better way to manage often used addresses?

You can use the "query" function in the index menu, which will call the
**$query\_command**.

For details: RTFM section **External Address Queries**

[QueryCommand](QueryCommand) provides a list of scripts for this purpose.
