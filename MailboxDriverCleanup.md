### Motivation

The mailbox drivers should be better separated into modules than is the
case now to allow for easier addition of new storage back ends (and, of
course, to have fewer \`switch\` statements and \`\#ifdef\` orgies.)

### Status

Most of the functionality is already modularized, but mx.c still
contains implementations of specific functions which it shouldn't -- it
should provide glue code mostly.

### Implementation

As a first step, the code in mx.c implementing mailbox-driver-specific
code should be moved to mh.c/mbox.c/imap. This should also count for the
mbox-related stuff in buffy.c for \`$check\_mbox\_size\`.

This probably means to add some new functions like
\`mx\_has\_newmail()\` or whatever to modularize mailbox polling. For
polling, we also maybe want to distinguish between drivers that work on
single mailboxes and those that work on groups and walk the incoming
list themselves. The latter would be the case for IMAP, but also for a
possible MAPI driver.

We also want a registry for drivers, i.e. a simple array of a driver
struct where \`ctx-\>magic\` is an index into. Adding the driver struct
pointer to CONTEXT at mailbox open time (instead of \`ctx-\>magic\`)
would be possible too, though we nowhere use such an OOP-like design.

### MAPI Support

The [OpenChange](http://www.openchange.org/) project provides a
\`libmapi\` for accessing MS exchange servers using native protocolls.
Such a driver doesn't seem to be too hard to implement, it should be
even easier once the mx drivers are more modular.

The bigger issue would be profile handling as we can't seem to just pass
auth and folder info via parameters to libmapi routines.
