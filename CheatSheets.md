## Printable Cheat Sheets

Here are some reference cards that can be printed out and kept near the
keyboard. Add more when you spot them. But always **remember**: you can
see all available keys for the mode you're currently in by **hitting
"?"**. Only exception is the editor mode (text input line), but you can
find **all** defaults in the manual.txt (or html), you can copy them
from there to build your own personal reference
card.

* Mutt Cheat Sheet, in PDF and Text  
  - <https://web.archive.org/web/20130203164545/http://media.commandline.org.uk/code/mutt.pdf> (18K 1 A4 page)  
  - <https://web.archive.org/web/20130814133423/http://media.commandline.org.uk/code/mutt.txt> (2.5K - 1 A4 page)

* Mutt Quick Reference Chart, in HTML  
  - <http://www.ucolick.org/~lharden/muttchart.html>  (9.4K - 4 A4 pages)
