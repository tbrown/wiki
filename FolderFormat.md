Mutt recognizes four [FolderFormat](FolderFormat)""s. If a folder is **correct** by any
of those formats' standard, then mutt can automatically read them,
nothing needs to be done just for reading. However, when you create new
folders ([MuttFaq/Action](MuttFaq/Action)), the format used is specified via
"\`mbox\_type\`":

* mbox  
* maildir  
* MH  
* MMDF

See [OverloadedNames](OverloadedNames) for clarification of the various uses of "mbox" and
"maildir".

## mbox

An mbox folder is a plain text file. Each message starts with a "From "
line (note the trailing space). For example:

`` `From roessler@does-not-xxx.invalid Fri Jun 23 02:56:55 2000` ``

Body-lines starting with "From" are usually escaped as "\>From" if they
occur in messages, but there is also a header "Content-Length:" that
gives the number of lines in the message so that From escaping is not
necessary. Mutt supports all formats.

The header is separated from the body by a blank line. See
<http://www.qmail.org/man/man5/mbox.html> for details.

## maildir

A maildir folder is a directory with **exactly three subdirectories**
"new", "cur", and "tmp". Each message is a separate file. See
[MaildirFormat](MaildirFormat) for details.

## MH

An MH folder is a directory containing either a .mh\_sequences or
.xmhcache file. Messages are separate files that are numbered
sequentially. MH is rarely used, most prefer the maildir format.

## MMDF

This is a variant of the mbox format. Each message is surrounded by
lines containing "^A^A^A^A" (four control-A's). Using this format is not
recommended, few other programs support it.

## Which format to use?

Don't use MH or MMDF, as these are not widely supported by other
programs. Whether to use mbox or maildir is a matter of taste. mbox is
sometimes easier to handle because it is just a single file, whereas
with maildir you don't need to read the whole folder just to get a
single message. The rest is philosophy, a big source of flamebaits, and
an endless quest for the best way to benchmark it. Maybe [MuttFaq/Maildir](MuttFaq/Maildir)
can help you out.

If you happend to choose either MH or Maildir, you can improve
performance by using header caching, see [MuttGuide/Caching](MuttGuide/Caching) for details.
