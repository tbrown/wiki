Welcome to the Mutt Wiki.

* Follow the style conventions on the page (if possible).  Try to have   
    1. a summary,   
    2. useful illustrations, and   
    3. links to the manual for all related sections (the manual is the authoritative document, the wiki is a non-authoritative guide to that document)

How to Edit

* Gitlab doesn't currently allow public edits to the wiki inside the "mutt" project.
* So instead, please clone the repos at https://gitlab.com/muttmua/wiki and create a Merge Request.
  We will then merge the request, and push it back here for you.
* Alternatively, you can submit a ticket with a description or a patch of the desired change.

When you edit/ add FAQs\&As,

* Try to avoid duplicating the same content on multiple pages, use an internal link to the (hopefully one) location instead  
* Use a fitting /Subpage to put your answers, use /Misc otherwise.  
* Create a new /Subpage as generic but specific as possible if there are related questions.  
* Bundle simple answers into /Subpages, put complex (lengthy) answers onto own separate pages.  
* The answers go to the /Subpages only  
* Remember to add the questions to the index list on the [MuttFaq](MuttFaq) page, too. They are not added automatically.  
* Instead of deleting content it out with so it can be fixed later if possible,  
  * domains sometimes change  
  * content may be duplicated elsewhere  
  * find similar functional content to replace the missing link content if possible  
* Use comments to point out problems, propose (radical) changes, indicate works in progress, etc.   
* Always fill out the "change information" comment box before submitting edits. Bigger changes should have better descriptions, and a rationale for the change if possible.

Persistence of information:

* If possible, don't just leave pointers to external web pages. Copy or append relevant content here in case the external site removes the page (or goes away).

Page management

* Use a fitting /Subpage to put your answers, use /Misc otherwise.
* Create a new /Subpage as generic but specific as possible if there are related questions.
* Bundle simple answers into /Subpages, put complex (lengthy) answers onto own separate pages.
* The answers go to the /Subpages only, not here.
* Remember to add the questions to any referring index list on the top page, too. They are not added automatically.

Providing updates to this wiki helps keep mutt usable for everyone.
